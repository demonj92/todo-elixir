defmodule TodoElixirWeb.Router do
  use TodoElixirWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :auth do
    plug(TodoElixir.Pipeline)
  end

  pipeline :ensure_auth do
    plug(Guardian.Plug.EnsureAuthenticated)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TodoElixirWeb do
    # Use the default browser stack
    pipe_through([:browser, :auth])

    get("/", PageController, :index)

    resources("/users", UserController)
  end

  scope "/session", TodoElixirWeb do
    pipe_through([:browser, :auth])
    get("/new", SessionController, :new)
    post("/login", SessionController, :login)
  end

  scope "/", TodoElixirWeb do
    pipe_through([:browser, :auth, :ensure_auth])
    get("/todos", TodoController, :index)
    delete("/session/logout", SessionController, :logout)
  end

  #  scope "/", TodoElixirWeb do
  #    pipe_through([:browser, :auth])
  #
  #    resources("/users", UserController, only: [:edit, :update, :show])
  #  end

  # Other scopes may use custom stacks.
  # scope "/api", TodoElixirWeb do
  #   pipe_through :api
  # end
end
