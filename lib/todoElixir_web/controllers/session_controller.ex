defmodule TodoElixirWeb.SessionController do
  use TodoElixirWeb, :controller

  alias TodoElixir.{User, Auth, Guardian}

  def new(conn, _params) do
    changeset = User.changeset(%User{})
    maybe_user = Guardian.Plug.current_resource(conn)

    if maybe_user do
      conn
      |> redirect("/")
    else
      conn
      |> render("new.html", changeset: changeset, action: session_path(conn, :login))
    end
  end

  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    Auth.authenticate_user(email, password)
    |> login_reply(conn)
  end

  def login_reply({:ok, user}, conn) do
    conn
    |> put_flash(:success, "Welcome back!")
    |> Guardian.Plug.sign_in(user)
    |> redirect(to: "/")
  end

  def login_reply({:error, reason}, conn) do
    conn
    |> put_flash(:error, to_string(reason))
    |> new(%{})
  end

  def logout(conn, _params) do
    conn
    |> Guardian.Plug.sign_out()
    |> redirect(to: "/")
  end
end
