defmodule TodoElixirWeb.UserController do
  use TodoElixirWeb, :controller

  alias TodoElixir.{Repo, User}

  def new(conn, _params) do
    changeset = User.changeset(%User{})

    conn
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)
    IO.inspect(changeset)

    case Repo.insert(changeset) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User created")
        |> redirect(to: page_path(conn, :index))

      {:error, changeset} ->
        conn
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id} = params) do
    user = Repo.get(User, id)
    changeset = User.changeset(user)

    conn
    |> render("edit.html", changeset: changeset, user: user)
  end

  def update(conn, %{"id" => user_id, "user" => user_params} = params) do
    user =
      User
      |> Repo.get(user_id)

    changeset = User.registration_changeset(user, user_params)
    IO.inspect(changeset)

    conn
    |> render("edit.html", changeset: changeset, user: user)
  end

  def show(conn, %{"id" => id}) do
    user = Repo.get(User, id)

    conn
    |> render("show.html", user: user)
  end
end
