defmodule TodoElixirWeb.PageController do
  use TodoElixirWeb, :controller
  alias TodoElixir.{Repo, User, Guardian}

  def index(conn, _params) do
    users = Repo.all(User)
    user = Guardian.Plug.current_resource(conn)
    IO.inspect(user)
    render(conn, "index.html", users: users, current_user: user)
  end
end
