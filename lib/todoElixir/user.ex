defmodule TodoElixir.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias TodoElixir.Repo
  alias Comeonin.Bcrypt

  schema "users" do
    field(:email, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :password_hash])
    |> validate_required([:email])
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/)
  end

  def registration_changeset(struct, params \\ %{}) do
    struct
    |> changeset(params)
    |> cast(params, [:password])
    |> validate_length(:password, min: 5)
    |> put_password_hash()
  end

  def get_user(id) do
    Repo.get(__MODULE__, id)
  end

  defp put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: pass}} = struct) do
    put_change(struct, :password_hash, Bcrypt.hashpwsalt(pass))
  end

  defp put_password_hash(struct), do: struct
end
