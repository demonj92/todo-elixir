defmodule TodoElixir.Auth do
  alias Comeonin.Bcrypt
  alias TodoElixir.{Repo, User}

  import Ecto.Query

  def authenticate_user(email, plain_password) do
    query = from(u in User, where: u.email == ^email)

    case Repo.one(query) do
      nil ->
        Bcrypt.dummy_checkpw()
        {:error, :invalid_credentials}

      user ->
        if(Bcrypt.checkpw(plain_password, user.password_hash)) do
          {:ok, user}
        else
          {:error, :invalid_credentials}
        end
    end
  end
end
