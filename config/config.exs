# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :todoElixir,
  ecto_repos: [TodoElixir.Repo]

# Configures the endpoint
config :todoElixir, TodoElixirWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "dZSxYr6l5Wqmkoqs6OLZJwGv3XRCfm1bseiCVvUFCBbuu04gGxj3Y+ApK58THUDk",
  render_errors: [view: TodoElixirWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TodoElixir.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :todoElixir, TodoElixir.Guardian,
  secret_key: "UWBWTx0dp1k1+VAXAhomFuHMAtSrWysinQo91+pPebdCLA0PIQAj242jFT9xW0ja",
  issuer: TodoElixir


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
